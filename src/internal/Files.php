<?php

namespace Archaic\Migrations\Internal;

class Files {

  private string $dir;

  public function __construct(string $dir) {
    $this->dir = realpath($dir);
  }

  public function existsRoot(): bool {
    return file_exists($this->dir);
  }

  public function existsFile(string $relativePath): bool {
    $path = join(DIRECTORY_SEPARATOR, [$this->dir, $relativePath]);
    return file_exists($path);
  }

  public function content(string $filepath): string {
    return file_get_contents($filepath);
  }

  public function find(string $relativeGlobPattern): array {
    $patternWithDir = join(DIRECTORY_SEPARATOR, [$this->dir, $relativeGlobPattern]);
    return glob($patternWithDir);
  }

  /** Returns files exists in dir but missing in given array. */
  public function missing(string $relativeGlobPattern, array $files): array {
    $having = $this->find($relativeGlobPattern);
    return array_diff($having, $files);
  }
}