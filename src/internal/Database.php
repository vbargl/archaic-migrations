<?php

namespace Archaic\Migrations\Internal;

use Archaic\PDO\PDO as ArchaicPDO;
use Kodus\SQL\Splitter;

use function Archaic\Log\printf;

class Database {

  private ArchaicPDO $pdo;

  public function __construct(ArchaicPDO $pdo) {
    $this->pdo = $pdo;
  }

  public function transaction(callable $fn) {
    $this->pdo->transaction($fn);
  }

  public function ensureSchema() {
    $this->pdo->command(
      "create table if not exists migrations(
        name varchar(255),
        version int,
        applied bool
      )");
  }

  public function clear() {
    $this->pdo->command("drop schema public cascade");
    $this->pdo->command("create schema public");
    $this->ensureSchema();
  }

  /** Migrate outside transaction. Each statement in file is executed alone.
   *  It can ends up in partial state. */
  public function unsafeMigrate(string $name, int $version, string $filepath) {
    $this->migrate($name, $version, $filepath);
  }

  /** Migrate insude transaction. Each statement in file is executed alone.
   *  It processes all statemenets successfully or none at all. */
  public function safeMigrate(string $name, int $version, string $filepath) {
    $this->pdo->transaction(function() use ($name, $version, $filepath) {
      $this->migrate($name, $version, $filepath);
    });
  }

  private function migrate(string $name, int $version, string $filepath) {
    if($this->isApplied($name)) {
      printf("migration %s is already applied", $name);
      return;
    }

    $statements = Splitter::split(file_get_contents($filepath));

    foreach ($statements as $stm) {
      $this->pdo->command($stm);
    }

    $this->pdo->insert("migrations", [
      'name'    => $name,
      'version' => $version,
      'applied' => true
    ]);
  }

  public function isApplied(string $name): bool {
    return $this->pdo->value(
      "select applied from migrations where name = :name", 
      compact('name')) === true;
  }

  public function version(): int {
    $version = $this->pdo->value(
      "select max(version) from migrations");

    if ($version != null) {
      return (int) $version;
    } else {
      return 0;
    }
  }

  public function files(): array {
    $files = $this->pdo->query(
      'select "name" from migrations',
      compact('version'));

    return array_column($files, "name");
  }
}