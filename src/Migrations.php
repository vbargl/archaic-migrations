<?php

namespace Archaic\Migrations;

use Archaic\PDO\PDO as ArchaicPDO;

use Archaic\Migrations\Internal\Database;
use Archaic\Migrations\Internal\Files;

class Migrations {
  private const GLOB_SQL_PATTERN = '*.sql';

  private Database $db;
  private Files    $files;

  public function __construct(ArchaicPDO $pdo, string $dir) {
    $this->db = new Database($pdo);
    $this->files = new Files($dir);
  }
  
  public function ensureSchema() {
    $this->db->ensureSchema();
  }

  public function migrate() {
    $files = $this->files->find(Migrations::GLOB_SQL_PATTERN);
    
    $this->db->transaction(function() use ($files) {
      $version = $this->db->version() + 1;

      foreach ($files as $file) {
        $name = basename($file);
        $this->db->unsafeMigrate($name, $version, $file);
      }
    });
  }

  public function refresh() {
    $this->db->clear();
    $this->migrate();
  }
}